#pragma once

// [[Custom header file for logging information in console ]]
// You can use C library with std::cout for logging or use
// custom header file (log.h).

#include "log.h"

// [[Example using callibri (folder 'callibri') ]]

#include "callibri.h"

// ==============
// || CALLIBRI ||
// ==============

void SampleCallibriFunction(Sensor* sensor_callibri);