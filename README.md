# NeuroSamples

The repository contains examples of using NeuroSDK 2 for all supported platforms. The examples show the capabilities of neurosdk in the field of searching, connecting and recieving a signal from BrainBit and Callibri devices.

Available now:
 - [x] Android
 - [ ] iOS 
 - [x] Unity
 - [x] .dotNet
 - [x] Python
 - [x] Xamarin
 - [x] ReactNative